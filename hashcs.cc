/*****************************************************/
/* HashCS v. 6.0.0

    FILE: hashcs.cc


    AUTHOR:
        Copyright (C) 2006-2009 Seung-Jin Sul
        Dept. of Computer Science
        Texas A&M University
      College Station, Texas, U.S.A.
        (contact: sulsj@cs.tamu.edu)

    HISTORY:

    06.13.2007 (v.1.0.0)

    06.14.2007 (v.1.0.1)
        - Add Consense::ConsenseTrees()

    06.21.2007 (v.1.0.3)
        - Add "delete bs_str" in HashMap::hashing_bs_without_type3_nbits_hashconsense()
          to fix memory leak
        - Use assign() in Consense::ConsenseTrees() to improve

    06.22.2007 (v.1.0.4)
        - Modify conversion routine in HashMap::hashing_bs_without_type3_nbits_hashconsense()
          to optimize

    08.06.2007 (v.1.0.6)
        - For new SC constructing algorithm

    08.16.2007
        - Consense.cpp optimization

    08.23.2007
        - New consensus tree construction routine completed.

    09.07.2007
        - Integration with 64-bit bitstring
        - vec_bucket = iter_hashcs->second; and vec_bucket[i]
          ===> (iter_hashcs->second).size(); // remove copy
        - hashmap.cc ==> hashing_bs_without_type3_64bits()

    09.11.2007 Hash function
        - hv.hv1 += _a1[i]*ii;
            hv.hv2 += _a2[i]*ii;
            ==>
            hv.hv1 += _a1[i];
            hv.hv2 += _a2[i];

    09.19.2007
        - New tree constructing routine
        - map<string, SCNode*> ==> map<string, unsigned>

    10.23.2007 Implicit bipartitions
        - Each node stores its hash code
        - If leaf, just get the a1[i] and a2[i] values
        - If internal node, get the childrens hash code and modular by m1 and m2.

    10.24.2007 Implicit BP is only applied with n-bit implementation

    10.27.2007 Optimization
        - Hashmap => vector
        - string bitstring in bucket structure => BitSet*
        - uhashfunc_init ==> two verisons (64-bit and n-bit)
        - vvecHashcs.resize()

    11.12.2007 DEBUG
        In hashfunc.cc --> top2 = c*t*n;

    11.13.2007 data type
          int --> unsigned
          uint64_t

    11.16.2007 DEBUG
                Prime number generator: fix fot the bigger number (from)


    06.01.2008 DEBUG
            combine mj and sc
            mj problem: distinct bs number is changing.
            support for multifurcating tree
            validate the result

    01.01.2009 Add option to specify the majority resolution rate

    02.26.2009 unsinged long long & RandomLib

    08.19.2009 bitstring collection -> for loop

    08.31.2009 user specified seed

    09.03.2009 DEBUG: for 64-bit machines
            PROBLEM: same taxon names exists more than one time in the resulting Newick string

    09.07.2009 Using STL bitset

    11.06.2009 UPDATE
            Collect n-3 biparitition.
            Update to deal with Grant's trees.
        Ready for 64-bit

    08.12.2010 Touch


*/
/*****************************************************/

//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.


//// My classes
#include "hashfunc.hh"
#include "hashmap.hh"
#include "SCTree.hh"
#include "SCNode.hh"

//// From Split-Dist
#include "label-map.hh"

//// Etc
#include <cassert>
#include <sys/time.h>
#include <sys/resource.h>

//// For newick parser
extern "C" {
#include <newick.h>
}

//// For command line arguments
#include <./tclap/CmdLine.h>
#include <bitset>


//// if double collision is reported, increase this!
//// the c value of m2 > c*t*n in the paper
static size_t  C               = 1000;
static size_t  NUM_TREES       = 0;         //// Number of trees
static size_t  NUM_TAXA        = 0;         //// Number of taxa
static float   RES_RATE        = 0.5;       //// Default resolution rate
static size_t  RES_NUMTREES    = 0;         //// boundary number of trees
static size_t  NEWSEED         = 0;         //// for storing user specified seed value


void
GetTaxaLabels2(
    NEWICKNODE *node,
    LabelMap &lm)
{
    if (node->Nchildren == 0) {
        string temp(node->label);
        lm.push(temp);
    }
    else
        for (size_t i = 0; i < node->Nchildren; ++i)
            GetTaxaLabels2(node->child[i], lm);
}

bitset<BITSETSZ> *
dfs_majority(
    NEWICKNODE *startNode,
    LabelMap &lm,
    HashMap &vvecHashcs,
    size_t treeIdx,
    uint64_t m1,
    uint64_t m2,
    vector<bitset<BITSETSZ> *> &vecBs,
    size_t &numBPCollected)
{
    if (treeIdx < RES_NUMTREES) {  //// treeIdx < NUM_TREES/2, if r=0.5 ==> do not collect bitstrings
        if (startNode->Nchildren == 0) { //// leaf node
            string temp(startNode->label);
            size_t idx = lm[temp];
            startNode->hv1 = vvecHashcs._HF.getA1(idx);
            startNode->hv2 = vvecHashcs._HF.getA2(idx);
            return NULL; 
        }
        else {   //// non-leaf node
            for (size_t i = 0; i < startNode->Nchildren; ++i) {
                dfs_majority(startNode->child[i], lm, vvecHashcs, treeIdx, m1, m2, vecBs, numBPCollected);
            }
            ++numBPCollected;
            uint64_t temp1 = 0;
            uint64_t temp2 = 0;
            for (size_t i = 0; i < startNode->Nchildren; ++i) {
                temp1 += startNode->child[i]->hv1;
                temp2 += startNode->child[i]->hv2;
            }
            startNode->hv1 = temp1 % m1;
            startNode->hv2 = temp2 % m2;
            //// Just populate the hash table with hv1 and hv2
            if (numBPCollected < NUM_TAXA - 2) {
                vvecHashcs.hashing_bs_MJ(startNode->hv1, startNode->hv2, RES_NUMTREES);
            }
            return NULL; 
        }
    }
    else { //// treeIdx >= NUM_TREES/2 ==> collect bipartitions
        if (startNode->Nchildren == 0) { // leaf node
            //// Get the location of taxon label in lm
            //// which is used to set "1" in bitstring.
            string temp(startNode->label);
            size_t idx = lm[temp];
            startNode->hv1 = vvecHashcs._HF.getA1(idx);
            startNode->hv2 = vvecHashcs._HF.getA2(idx);
            //// Here we make an actual bitstring
            bitset<BITSETSZ> *bs = new bitset<BITSETSZ>;
            //// bitset is little endian
            (*bs)[BITSETSZ-1-idx] = true;
            int numOnes = 0;
            numOnes = (*bs).count();
            if (numOnes > 1 || numOnes == 0) {
                cout << "numOnes = " << numOnes << endl;
                cerr << "ERROR: bitstring creation error\n";
                exit(0);
            }
            return bs; //// This is actual bistring
        }
        else { //// non-leaf node
            bitset<BITSETSZ> *ebs[startNode->Nchildren];
            for (size_t i = 0; i < startNode->Nchildren; ++i) {
                ebs[i] = dfs_majority(startNode->child[i], lm, vvecHashcs, treeIdx, m1, m2, vecBs, numBPCollected);
            }
            //// At this point, we find a bipartition.
            //// Thus, OR the bitstrings and make a bit string for the bipartition
            bitset<BITSETSZ> *bs = new bitset<BITSETSZ>;
            for (size_t i = 0; i < startNode->Nchildren; ++i) {
                if (ebs[i]) {
                    *bs |= *(ebs[i]); //// This bit operation creates bitstring for a internal node.
                    delete ebs[i];
                    ebs[i] = NULL;
                }
                else {
                    cout << "ERROR: null bitstring\n";
                    exit(0);
                }
            }
            int numOnes = 0;
            numOnes = (*bs).count();
            if (numOnes < 1) {
                cout << "numOnes = " << numOnes << endl;
                cerr << "ERROR: bitstring OR error\n";
                exit(0);
            }
            ++numBPCollected;
            uint64_t temp1 = 0;
            uint64_t temp2 = 0;
            for (size_t i = 0; i < startNode->Nchildren; ++i) {
                temp1 += startNode->child[i]->hv1;
                temp2 += startNode->child[i]->hv2;
            }
            startNode->hv1 = temp1 % m1;
            startNode->hv2 = temp2 % m2;
            //// Here we need to store the actial bitsting collected.
            //// *bs is the bitstring. In hashing_bs_MJ, the bitstring is checked if it is
            //// majority bipartition or not. If yes, the bipartition is stored in vecBs.
            //// vecBs is later used for constructing majority consensus tree.
            if (numBPCollected < NUM_TAXA - 2) {
                vvecHashcs.hashing_bs_MJ(*bs, NUM_TAXA, startNode->hv1, startNode->hv2, vecBs, RES_NUMTREES);
            }
            return bs;
        }
    }
}


bitset<BITSETSZ> *
dfs_strict(
    NEWICKNODE *startNode,
    LabelMap &lm,
    HashMap &vvecHashcs,
    size_t treeIdx,
    uint64_t m1,
    uint64_t m2,
    vector<bitset<BITSETSZ> *> &vecBs,
    size_t &numBPCollected)
{
    if (treeIdx == 0) { //// for the first tree ==> collect bipartitions
        //// If the node is leaf node, just set the place of the taxon name in the bit string to '1'
        //// and push the bit string into stack
        if (startNode->Nchildren == 0) { // leaf node
            //// Get the location of taxon label in lm
            //// which is used to set "1" in bitstring.
            string temp(startNode->label);
            size_t idx = lm[temp];
            //// Implicit BPs /////////////////////////
            //// Set the hash values for each leaf node.
            startNode->hv1 = vvecHashcs._HF.getA1(idx);
            startNode->hv2 = vvecHashcs._HF.getA2(idx);
            //// Here we make an actual bitstring
            //BitSet* bs = new BitSet(NUM_TAXA);
            bitset<BITSETSZ> * bs = new bitset<BITSETSZ>;
            (*bs)[BITSETSZ-1-idx] = true;
            size_t numOnes = 0;
            numOnes = (*bs).count();
            if (numOnes > 1 || numOnes == 0) {
                cout << "numOnes = " << numOnes << endl;
                cerr << "ERROR: bitstring creation error\n";
                exit(0);
            }
            return bs; //// return the bitstring for this leaf node (=taxon)
        }
        else { //// non-leaf node
            //BitSet* ebs[startNode->Nchildren];
            bitset<BITSETSZ> * ebs[startNode->Nchildren];
            for (size_t i = 0; i < startNode->Nchildren; ++i) {
                ebs[i] = dfs_strict(startNode->child[i], lm, vvecHashcs, treeIdx, m1, m2, vecBs, numBPCollected);
            }
            //// At this point, we find a bipartition.
            //// Thus, OR the bitstrings and make a bit string for the bipartition
            //BitSet* bs = new BitSet(NUM_TAXA);
            bitset<BITSETSZ> * bs = new bitset<BITSETSZ>;
            for (size_t i = 0; i < startNode->Nchildren; ++i) {
                if (ebs[i]) {
                    *bs |= *(ebs[i]);
                    delete ebs[i];
                    ebs[i] = NULL;
                }
                else {
                    cout << "ERROR: null bitstring\n";
                    exit(0);
                }
            }
            int numOnes = 0;
            numOnes = (*bs).count();
            if (numOnes < 1) {
                cout << "numOnes = " << numOnes << endl;
                cerr << "ERROR: bitstring OR error\n";
                exit(0);
            }
            ++numBPCollected;
            //// Implicit BPs ////////////
            //// After an internal node is found, compute the hv1 and hv2
            uint64_t temp1 = 0;
            uint64_t temp2 = 0;
            for (size_t i = 0; i < startNode->Nchildren; ++i) {
                temp1 += startNode->child[i]->hv1;
                temp2 += startNode->child[i]->hv2;
            }
            startNode->hv1 = temp1 % m1;
            startNode->hv2 = temp2 % m2;
            //// Store bit strings in hash table
            //// Here we need to store the actial bitsting collected.
            //// *bs is the bitstring. In hashing_bs_SC, the bitstring is checked if it is
            //// strict bipartition or not. If yes, the bipartition is stored in vecBs.
            //// vecBs is later used for constructing strict consensus tree.
            //// vvecHashcs.hashing_bs_SC(*bs, treeIdx, NUM_TAXA, NUM_TREES, startNode->hv1, startNode->hv2, vecBs);
            if (numBPCollected < NUM_TAXA - 2) {
                vvecHashcs.hashing_bs_SC(*bs, NUM_TAXA, startNode->hv1, startNode->hv2);
            }
            return bs;
        }
    }
    else {   //// for the other trees
        if (startNode->Nchildren == 0) { //// leaf node
            string temp(startNode->label);
            size_t idx = lm[temp];
            //// Implicit BPs /////////////////////////
            //// Set the hash values for each leaf node.
            startNode->hv1 = vvecHashcs._HF.getA1(idx);
            startNode->hv2 = vvecHashcs._HF.getA2(idx);
            return NULL;
        }
        else {  //// non-leaf node
            for (size_t i = 0; i < startNode->Nchildren; ++i) {
                dfs_strict(startNode->child[i], lm, vvecHashcs, treeIdx, m1, m2, vecBs, numBPCollected);
            }
            ++numBPCollected;
            //// Implicit BPs ////////////
            //// After an internal node is found, compute the hv1 and hv2
            uint64_t temp1 = 0;
            uint64_t temp2 = 0;
            for (size_t i = 0; i < startNode->Nchildren; ++i) {
                temp1 += startNode->child[i]->hv1;
                temp2 += startNode->child[i]->hv2;
            }
            startNode->hv1 = temp1 % m1;
            startNode->hv2 = temp2 % m2;
            //// Store bit strings in hash table
            //vvecHashcs.hashing_bs_SC(*bs, treeIdx, NUM_TAXA, NUM_TREES, startNode->hv1, startNode->hv2, vecBs);
            if (numBPCollected < NUM_TAXA - 2) {
                vvecHashcs.hashing_bs_SC(treeIdx, NUM_TAXA, NUM_TREES, startNode->hv1, startNode->hv2, vecBs);
            }
            return NULL;
        }
    }
}


string itos(int i)  //// convert int to string
{
    stringstream s;
    s << i;
    return s.str();
}


int main(int argc, char** argv)
{
    string outfilename;
    bool f_mj = false;
    //// TCLAP
    try {
        //// Define the command line object.
        string  helpMsg  = "HashCS\n";
        helpMsg += "Input file: \n";
        helpMsg += "   The current version of HashCS only supports the Newick format.\n";
        helpMsg += "Example of Newick tree: \n";
        helpMsg += "   (('Chimp':0.052625,'Human':0.042375):0.007875,'Gorilla':0.060125,\n";
        helpMsg += "   ('Gibbon':0.124833,'Orangutan':0.0971667):0.038875);\n";
        helpMsg += "   ('Chimp':0.052625,('Human':0.042375,'Gorilla':0.060125):0.007875,\n";
        helpMsg += "   ('Gibbon':0.124833,'Orangutan':0.0971667):0.038875);\n";
        helpMsg += "File option: (default = outtree)\n";
        helpMsg += "   -o <outfile name>, specify a file name to save the result tree.\n";
        helpMsg += "consensus tree option: (default = strict consensus)\n";
        helpMsg += "   -j, for computing majority consensus tree";
        helpMsg += "consensus tree resolution rate: (for majority consensus)\n";
        helpMsg += "   -r <rate>, specify resolution rate between 0 and 100.  \n";
        helpMsg += "Specify c value: \n";
        helpMsg += "   -c <rate>, specify c value (default: 1000) \n";
        helpMsg += "Examples: \n";
        helpMsg += "  hashcs foo.tre 1000 \n";
        helpMsg += "  hashcs foo.tre 1000 -o out.tre\n";
        helpMsg += "  hashcs foo.tre 1000 -j -o out.tre\n";
        helpMsg += "  hashcs foo.tre 1000 -j -r 50 -o out.tre\n";
        helpMsg += "  hashcs foo.tre 1000 -j -r 50 -s 2000 -o out.tre\n";
        TCLAP::CmdLine cmd(helpMsg, ' ', "6.0.0");
        //// fnameArg is not used actually.
        TCLAP::UnlabeledValueArg<string>  fnameArg("name", "file name", true, "intree", "Input tree file name");
        cmd.add(fnameArg);
        TCLAP::UnlabeledValueArg<size_t>  numtreeArg("numtree", "number of trees", true, 2, "Number of trees");
        cmd.add(numtreeArg);
        TCLAP::SwitchArg mjSwitch("j", "majority", "Compute majority consensus tree", false);
        cmd.add(mjSwitch);
        TCLAP::ValueArg<size_t> cArg("c", "cvalue", "c value", false, 1000, "c value");
        cmd.add(cArg);
        TCLAP::ValueArg<size_t> rArg("r", "rvalue", "rate value", false, 50, "rate value");
        cmd.add(rArg);
        TCLAP::ValueArg<string> outfileArg("o", "outfile", "Output file name", false, "outtree", "Output file name");
        cmd.add(outfileArg);
        //// debug
        TCLAP::ValueArg<size_t> seedArg("s", "seedvalue", "user specified seed value", false, 1000, "user specified seed value");
        cmd.add(seedArg);
        cmd.parse(argc, argv);
        NUM_TREES = numtreeArg.getValue();
        if (NUM_TREES == 0) {
            char strFileLine[100000]; //// One line buffer...
            unsigned long ulLineCount;
            ulLineCount = 0;
            ifstream infile;
            infile.open(argv[1]);
            if (infile) {
                while (infile.getline(strFileLine, sizeof(strFileLine), '\n')) {
                    ulLineCount++;
                }
            }
            cout << "*** Number of trees in the input file: " << ulLineCount << endl;
            NUM_TREES = ulLineCount;
            infile.close();
            delete [] strFileLine;
        }
        if (NUM_TREES < 2) {
            cerr << "Fatal error: at least two trees expected.\n";
            exit(2);
        }
        f_mj = mjSwitch.getValue();
        if (seedArg.getValue())
            NEWSEED = seedArg.getValue();
        if (cArg.getValue())
            C = cArg.getValue();
        if (rArg.getValue()) {
            if (rArg.getValue() > 0 && rArg.getValue() <= 100) {
                if (f_mj) {
                    RES_RATE = float(rArg.getValue()) / 100.0;
                    RES_NUMTREES = NUM_TREES * RES_RATE;
                    if (RES_NUMTREES == NUM_TREES)
                        f_mj = false;
                }
                //else {
                //cout << "WARNING: -r option only works with -j (majority consensus tree). The current value will be ignored.\n";
                //}
            }
            else {
                cout << "ERROR: 0 < rate <= 100\n";
                exit(0);
            }
        }
        outfilename = outfileArg.getValue();
    }
    catch (TCLAP::ArgException &e) { //// catch any exceptions
        cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
    }
    /*********************************************************************************/
    //cout << "*** Reading a tree file and parsing the tree for taxon label collection ***\n";
    /*********************************************************************************/
    NEWICKTREE *newickTree;
    int err;
    FILE *fp;
    fp = fopen(argv[1], "r");
    if (!fp) {
        cout << "ERROR: file open error\n";
        exit(0);
    }
    newickTree = loadnewicktree2(fp, &err);
    if (!newickTree) {
        switch (err) {
        case -1:
            printf("Out of memory\n");
            break;
        case -2:
            printf("parse error\n");
            break;
        case -3:
            printf("Can't load file\n");
            break;
        default:
            printf("Error %d\n", err);
        }
    }
    /*********************************************************************************/
    cout << "\n*** Collecting the taxon labels ***\n";
    /*********************************************************************************/
    //// traverse a tree in dfs order and collect taxon label in lm.
    //// This list is used for determining the location index of taxon label
    //// when generating n-bit bitstring.
    LabelMap lm;
    try {
        GetTaxaLabels2(newickTree->root, lm);
    }
    catch (LabelMap::AlreadyPushedEx ex) {
        cerr << "ERROR: The label '" << ex.label << "' appeard twice in " << endl;
        exit(2);
    }
    NUM_TAXA = lm.size();
    cout << "    Number of taxa = " << NUM_TAXA << endl;
    killnewicktree(newickTree);
    fclose(fp);
    /*********************************************************************************/
    cout << "\n*** Reading trees and collecting bipartitions ***\n";
    /********************************************************************************/
    //// Class HashRFMap for HashConsense
    HashMap vvecHashcs;
    //// Init hash function class
    uint64_t M1 = 0;
    uint64_t M2 = 0;
    //// If there is a user specified seed, use the value for initializing
    //// random number generator for hash functions.
    if (NEWSEED != 1000)
        vvecHashcs.UHashfunc_init(NUM_TREES, NUM_TAXA, C, NEWSEED);
    else
        vvecHashcs.UHashfunc_init(NUM_TREES, NUM_TAXA, C);
    M1 = vvecHashcs._HF.getM1();
    M2 = vvecHashcs._HF.getM2();
    //// Increase the size of hash table to M1
    vvecHashcs._hashTab.resize(M1);
    //// to store collected strict bipartitions
    //// NOTE: strict/majority bipartitions are not stored in hash table.
    vector<bitset<BITSETSZ> *> vecBs;
    assert(vecBs.size() == 0);
    fp = fopen(argv[1], "r");
    if (!fp) {
        cout << "ERROR: file open error\n";
        exit(0);
    }
    if (f_mj) { //// MAJORITY
        for (size_t treeIdx = 0; treeIdx < NUM_TREES; ++treeIdx) {
            newickTree = loadnewicktree2(fp, &err);
            if (!newickTree) {
                switch (err) {
                case -1:
                    printf("Out of memory\n");
                    break;
                case -2:
                    printf("parse error\n");
                    break;
                case -3:
                    printf("Can't load file\n");
                    break;
                default:
                    printf("Error %d\n", err);
                }
            }
            else {
                size_t numBPCollected = 0;
                dfs_majority(newickTree->root, lm, vvecHashcs, treeIdx, M1, M2, vecBs, numBPCollected);
                killnewicktree(newickTree);
            }
        }
    }
    else { //// STRICT
        for (size_t treeIdx = 0; treeIdx < NUM_TREES; ++treeIdx) {
            newickTree = loadnewicktree2(fp, &err);
            if (!newickTree) {
                switch (err) {
                case -1:
                    printf("Out of memory\n");
                    break;
                case -2:
                    printf("parse error\n");
                    break;
                case -3:
                    printf("Can't load file\n");
                    break;
                default:
                    printf("Error %d\n", err);
                }
            }
            else {
                size_t numBPCollected = 0;
                dfs_strict(newickTree->root, lm, vvecHashcs, treeIdx, M1, M2, vecBs, numBPCollected);
                killnewicktree(newickTree);
            }
        }
    }
    cout << "    Number of trees = " << NUM_TREES << endl;
    fclose(fp);
    //// This is to collect SCNode* for bitsting
    //// For example: when bitstring = 11001
    //// 1. Get the taxon label of '1's => 1st 2nd and 5th
    //// 2. Make 3 SCNode* and insert the pointer in this data structure
    vector<vector<SCNode*> > vvec_distinctClusters2;
    bitset<BITSETSZ> *bs2 = new bitset<BITSETSZ>;
    (*bs2).flip();
    vecBs.push_back(bs2);
    //// NOTE: now vecBs has all strict or majority bipartitions collected.
    //// Next step is to construct trees from the bitstring information.
    //// Using vvec_distinctClusters2, make a list of SCNode* for each bipartition.
    for (size_t i = 0; i < vecBs.size(); ++i) {
        vector<SCNode*> vec_nodes2;
        size_t lmIndex = 0;
        for (size_t j = BITSETSZ - 1; j > BITSETSZ - NUM_TAXA - 1; --j) {
            if ((*(vecBs[i]))[j]) {
                SCNode* aNode = new SCNode();
                aNode->name = lm.name(lmIndex);
                vec_nodes2.push_back(aNode);
            }
            lmIndex++;
        }
        vvec_distinctClusters2.push_back(vec_nodes2);
    }
    //// WE'RE GOING TO INGNORE THE LAST BIPARTITIONS
    float numbps = vvec_distinctClusters2.size() - 1;
    //// TO COMPUTE RESOLUTION RATE
    float denom = NUM_TAXA - 3;
    float resol = numbps / denom;
    cout << "    Resolution rate (%) = " << resol * 100 << " ( " << numbps << " / " << NUM_TAXA - 3 << " * 100)" << endl;
    //// clear hash table and clear bistrings collected
    vvecHashcs.HashMap_clear();
    vecBs.clear();
    //// If there is no common bipartition b/w trees.
    //// (to construct star tree)
    if (vvec_distinctClusters2.size() == 0) {
        vector<SCNode*> vec_nodes2;
        for (size_t j = 0; j < lm.size(); ++j) {
            SCNode* aNode = new SCNode();
            aNode->name = lm.name(j);
            vec_nodes2.push_back(aNode);
        }
        vvec_distinctClusters2.push_back(vec_nodes2);
    }
    //// Create multimap with size of the cluster as key.
    //// This is to reorder the list of SCNode* by the number of '1's
    //// in the original bitstirng.
    //// If we process the largest (= biggest number of '1's) first
    //// that makes easy to consruct consensus trees.
    multimap<size_t, size_t, greater<size_t> > mmap_cluster;
    //// Insert the size of distict vector and the index
    //// To sort the distinct vectors by the size of clusters
    for (size_t i = 0; i < vvec_distinctClusters2.size(); ++i)
        mmap_cluster.insert(multimap<size_t, size_t>::value_type(vvec_distinctClusters2[i].size(), i));
    /*********************************************************************************/
    cout << "\n*** Construct ";
    if (f_mj)
        cout << "MAJORITY ";
    else
        cout << "STRICT ";
    cout << "consensus tree ***\n";
    if (f_mj)
        cout << "    Resolution rate = " << RES_RATE * 100 << " (%)" << "\n";
    /*********************************************************************************/
    //////////////////////////////////////////////////////////////////////////////
    //// 09.19.2007
    //// Construct SC tree
    //////////////////////////////////////////////////////////////////////////////
    multimap<size_t, size_t>::iterator itr;
    SCTree *scTree = new SCTree();
    bool addedRoot = false;
    size_t intNodeNum = 0;
    vector<SCNode*> vec_garbageCan;
    for (itr = mmap_cluster.begin(); itr != mmap_cluster.end(); ++itr) {
        if (!addedRoot) {
            //// The first cluster has all the taxa.
            //// This constructs a star tree with all the taxa.
            //// 1. Dangle all the taxa as root's children by adjusting parent link.
            //// 2. Push all the node* in the root's children
            //// 3. Push all the nodes in the tree's nodeList.
            //// 4. Push all the nodes' parent in the tree's parentlist.
            for (size_t i = 0; i < vvec_distinctClusters2[itr->second].size(); ++i) {
                vvec_distinctClusters2[itr->second][i]->parent = scTree->root;
                scTree->root->children.push_back(vvec_distinctClusters2[itr->second][i]);
                scTree->nodeList.push_back(vvec_distinctClusters2[itr->second][i]);
                assert(scTree->nodeList[0]->name == "root");
                scTree->parentList.insert(map<string, size_t>::value_type(vvec_distinctClusters2[itr->second][i]->name, 0));
            }
            addedRoot = true;
        }
        else {
            //// For the next biggest cluster,
            //// 1. Find node list to move (= vvec_distinctClusters2[itr->second]) and
            ////    Get the parent node of the to-go nodes.
            //// 2. Make an internal node.
            //// 3. Insert the node in the nodeList of the tree and update the parentlist accordingly
            //// 4. Adjust the to-go nodes' parent link.
            //// 5. Adjust the parent node's link to children (delete the moved nodes from children).
            //// 1. --------------------------------------------------------------------------
            SCNode* theParent = NULL;
            theParent = scTree->nodeList[scTree->parentList[vvec_distinctClusters2[itr->second][0]->name]];
            assert(theParent != NULL);
            assert(theParent->name != "");
            //// 2. --------------------------------------------------------------------------
            //string newIntNodeName = "int" + itostr(intNodeNum, 10);
            string newIntNodeName = "int" + itos(intNodeNum);
            SCNode* newIntNode = new SCNode();
            vec_garbageCan.push_back(newIntNode);
            newIntNode->name = newIntNodeName;
            newIntNode->parent = theParent;
            //// 3. --------------------------------------------------------------------------
            assert(newIntNodeName.size() != 0);
            scTree->nodeList.push_back(newIntNode);
            assert(scTree->nodeList[scTree->nodeList.size()-1]->name == newIntNode->name);
            scTree->parentList.insert(map<string, size_t>::value_type(newIntNodeName, scTree->nodeList.size() - 1));
            for (size_t i = 0; i < vvec_distinctClusters2[itr->second].size(); ++i) {
                //// 4. --------------------------------------------------------------------------
                vvec_distinctClusters2[itr->second][i]->parent = newIntNode;
                //// We have to update parentlist in the tree.
                assert(vvec_distinctClusters2[itr->second][i]->parent->name == scTree->nodeList[scTree->nodeList.size()-1]->name);
                scTree->parentList[vvec_distinctClusters2[itr->second][i]->name] = scTree->nodeList.size() - 1;
                newIntNode->children.push_back(vvec_distinctClusters2[itr->second][i]);
                //// 5. --------------------------------------------------------------------------
                //// Delete the moved nodes from parent's children.
                vector<SCNode*>::iterator itr2;
                for (itr2 = theParent->children.begin(); itr2 != theParent->children.end(); ++itr2) {
                    if (vvec_distinctClusters2[itr->second][i]->name == (*itr2)->name) {
                        theParent->children.erase(itr2);
                        break;
                    }
                }
            }
            theParent->children.push_back(newIntNode);
            intNodeNum++;
        }
        //scTree->DrawOnTerminal();
    }
    /*********************************************************************************/
    cout << "\n*** Save ";
    if (f_mj)
        cout << "MAJORITY ";
    else
        cout << "STRICT ";
    cout << "consensus tree to ";
    if (outfilename != "outtree")
        cout << outfilename;
    else
        cout << "\"outtree\"";
    cout << " file ***\n";
    /*********************************************************************************/
    ofstream fout;
    if (outfilename != "outtree")
        fout.open(outfilename.c_str());
    else
        fout.open("outtree");
    //// remove out the extra parenthesis from the Newick string.
    string newickStr = scTree->GetTreeString();
    //scTree->DeleteAllNodes();
    for (size_t i = 0; i < vec_garbageCan.size(); ++i) {
        if (vec_garbageCan[i]) {
            SCNode * temp = vec_garbageCan[i];
            delete temp;
            vec_garbageCan[i] = NULL;
        }
    }
    vec_garbageCan.clear();
    //string newickStr2(newickStr, 1, newickStr.length()-3);
    //fout << newickStr2 + ";";
    fout << newickStr << endl;
    fout.close();
    cout << "    Completed!\n";
    mmap_cluster.clear();
    for (size_t i = 0; i < vvec_distinctClusters2.size(); ++i)
        vvec_distinctClusters2[i].clear();
    //// clear cluster table
    vvec_distinctClusters2.clear();
    //// CPU time comsumed
    struct rusage a;
    if (getrusage(RUSAGE_SELF, &a) == -1) {
        cerr << "ERROR: getrusage failed.\n";
        exit(2);
    }
    cout << "\n    Total CPU time: " << a.ru_utime.tv_sec + a.ru_stime.tv_sec << " sec and ";
    cout << a.ru_utime.tv_usec + a.ru_stime.tv_usec << " usec.\n";
    return 1;
}


