#include "SCTree.hh"
#include "SCNode.hh"
#include <iostream>
#include <cassert>

SCNode::SCNode()
{
    parent = NULL;
    ClearChildren();
    support = 0;
}

SCNode::~SCNode()
{
    //// Delete Nodes in the Tree
    //    for (size_t i = 0; i < children.size(); ++i) {
    //        if (children[i] != NULL) {
    //            delete children[i];
    //            children[i] = NULL;
    //        }
    //    }
}

void
SCNode::ClearChildren()
{
    for (size_t i = 0; i < children.size(); ++i)
        children[i] = NULL;
}

size_t
SCNode::NumChildren()
{
    size_t cnt = 0;
    for (size_t i = 0; i < children.size(); ++i)
        if (children[i] != NULL) cnt++;
    return cnt;
}

void
SCNode::SetDistance(double distance)
{
    //// One of manu's datasets gives us intermediate negative bl
    if (distance < 0)
        bl = 0.0;
    else
        bl = distance;
}

double
SCNode::GetDistance() const
{
    return bl;
}

void SCNode::DrawOnTerminal(int dp, bool distances)
{
    int i = 0;
    while (i < dp) {
        cout << "    ";
        ++i;
    }
    cout << name;
    //  if (support)
    //      cout << " (" << support << ")";
    //  if (distances)
    //      cout << " " << bl;
    cout << endl;
    for (size_t i = 0; i < NumChildren(); ++i)
        children[i]->DrawOnTerminal(dp + 1, distances);
}

bool
SCNode::IsRoot()
{
    return (parent == NULL);
}

bool
SCNode::IsLeaf()
{
    return (NumChildren() == 0);
}

