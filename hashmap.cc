///*****************************************************/
//
// Copyright (C) 2006-2009 Seung-Jin Sul
//      Department of Computer Science
//      Texas A&M University
//      Contact: sulsj@cs.tamu.edu
//
//      CLASS IMPLEMENTATION
//      HashMap: Class for hashing bit
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details (www.gnu.org).
//
//*****************************************************************/

#include "hashmap.hh"
#include "hashfunc.hh"

//// For populating hash table
//// It does not store bitstrings.
void
HashMap::hashing_bs_MJ(
    uint64_t hv1,
    uint64_t hv2,
    size_t resNumTrees)
{
    if (_hashTab[hv1].empty()) {
        BUCKET_STRUCT_T_HASHCS bk;
        bk._hv2 = hv2;
        bk._bs2 = NULL;
        bk._count = 1;
        _hashTab[hv1].push_back(bk);
    }
    else {
        bool found = false;
        for (size_t i = 0; i < _hashTab[hv1].size(); ++i) {
            if (_hashTab[hv1][i]._hv2 == hv2) {
                found = true;
                if (_hashTab[hv1][i]._count <= resNumTrees) { //// filter for increasement
                    _hashTab[hv1][i]._count++; //// Increase the occurrence count
                }
                break;
            }
        }
        if (!found) {
            BUCKET_STRUCT_T_HASHCS bk;
            bk._hv2 = hv2;
            bk._bs2 = NULL;
            bk._count = 1;
            _hashTab[hv1].push_back(bk);
        }
    }
}



//// For comparing hv1 and hv2 and storing bitstrings.
void
HashMap::hashing_bs_MJ(
    bitset<BITSETSZ> &bs,
    size_t numTaxa,
    uint64_t hv1,
    uint64_t hv2,
    vector<bitset<BITSETSZ> *> &vecBs,
    size_t resNumTrees)
{
    if (!_hashTab[hv1].empty()) {
        bool found = false;
        for (size_t i = 0; i < _hashTab[hv1].size(); ++i) {
            if (_hashTab[hv1][i]._hv2 == hv2) {
                if (_hashTab[hv1][i]._count <= resNumTrees) {
                    _hashTab[hv1][i]._count++; //// Increase the occurrence count
                    //// now we find a majority bipartition
                    //// save the bitstring in vecBs
                    if (_hashTab[hv1][i]._count > resNumTrees) {
                        bitset<BITSETSZ> *tempbs = new bitset<BITSETSZ>;
                        *tempbs = bs;
                        vecBs.push_back(tempbs);
                    }
                }
                found = true;
                break;
            }
        }
    }
}



void
HashMap::hashing_bs_SC(
    bitset<BITSETSZ>& bs,
    size_t numTaxa,
    uint64_t hv1,
    uint64_t hv2)
{
    BUCKET_STRUCT_T_HASHCS bk;
    bk._hv2 = hv2;
    bitset<BITSETSZ> *tempbs = new bitset<BITSETSZ>;
    *tempbs = bs;
    bk._bs2 = tempbs;
    bk._count = 1;
    _hashTab[hv1].push_back(bk);
}


void
HashMap::hashing_bs_SC(
    size_t treeIdx,
    size_t numTaxa,
    size_t numTrees,
    uint64_t hv1,
    uint64_t hv2,
    vector<bitset<BITSETSZ> *> &vecBs)
{
    //// The other trees: just compare the biaprtitions in the hashtable
    if ((treeIdx > 0) & (treeIdx < numTrees - 1)) {
        if (!_hashTab[hv1].empty()) { // if the hash table location has already been occupied
            //// get the number of items in the linked list
            for (size_t i = 0; i < _hashTab[hv1].size(); ++i) {
                if (_hashTab[hv1][i]._hv2 == hv2) { // if the same bp is found
                    _hashTab[hv1][i]._count++;
                    break;
                }
            }
        }
    }
    //// if treeIdx == numTrees ?????????????????
    else {
        if (!_hashTab[hv1].empty()) {
            for (size_t i = 0; i < _hashTab[hv1].size(); ++i) {
                if (_hashTab[hv1][i]._hv2 == hv2) {
                    //// now we find strict bipartition
                    //// store the bitstring in vecBs
                    if (_hashTab[hv1][i]._count == numTrees - 1)
                        vecBs.push_back(_hashTab[hv1][i]._bs2);
                    break;
                }
            }
        }
    }
}


void
HashMap::HashMap_clear()
{
    for (size_t i = 0; i < _hashTab.size(); ++i) {
        for (size_t j = 0; j < _hashTab[i].size(); ++j) {
            if (_hashTab[i][j]._bs2) {
                delete _hashTab[i][j]._bs2;
                _hashTab[i][j]._bs2 = NULL;
            }
            _hashTab[i].clear();
        }
    }
    _hashTab.clear();
}

void
HashMap::UHashfunc_init(
    size_t t, /// number of trees
    size_t n, //// number of taxa
    size_t c) //// c value in m2 > c*t*n
{
    _HF.UHashfunc_init(t, n, c);
}


void
HashMap::UHashfunc_init(
    size_t t,           //// number of trees
    size_t n,           //// number of taxa
    size_t c,           //// c value in m2 > c*t*n
    size_t newseed)     //// user specified seed
{
    _HF.UHashfunc_init(t, n, c, newseed);
}



