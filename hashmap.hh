//*****************************************************************/
//
// Copyright (C) 2006-2009 Seung-Jin Sul
//      Department of Computer Science
//      Texas A&M University
//      Contact: sulsj@cs.tamu.edu
//
//      CLASS DEFINITION
//      HashMap: Class for hashing
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details (www.gnu.org).
//
//*****************************************************************/

#ifndef HASHMAP_HH
#define HASHMAP_HH

#include <vector>
#include <bitset>

#include "hashfunc.hh"

using namespace std;


// For HashConsense

typedef struct {
    uint64_t            _hv2;
    bitset<BITSETSZ>    *_bs2;
    unsigned long _count;
} BUCKET_STRUCT_T_HASHCS;

typedef vector<BUCKET_STRUCT_T_HASHCS> V_BUCKET_T_HASHCS;
typedef vector<V_BUCKET_T_HASHCS> HASHTAB_T_HASHCS;

class HashMap {
public:

    HashMap() {
    }

    ~HashMap() {
    }

    HASHTAB_T_HASHCS _hashTab;
    CHashFunc _HF;

    void hashing_bs_MJ(uint64_t hv1, uint64_t hv2, size_t res_numtrees);
    void hashing_bs_MJ(bitset<BITSETSZ>& bs, size_t numTaxa, uint64_t hv1, uint64_t hv2, vector<bitset<BITSETSZ> *> & vecBs, size_t res_numtrees);

    void hashing_bs_SC(size_t treeIdx, size_t numTaxa, size_t numTrees, uint64_t hv1, uint64_t hv2, vector<bitset<BITSETSZ> *> & vecBs);
    void hashing_bs_SC(bitset<BITSETSZ>& bs, size_t numTaxa, uint64_t hv1, uint64_t hv2);

    void UHashfunc_init(size_t t, size_t n, size_t c);
    void UHashfunc_init(size_t t, size_t n, size_t c, size_t newSseed);
    void HashMap_clear();
};


#endif


