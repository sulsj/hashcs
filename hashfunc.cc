///*****************************************************/
//
// Copyright (C) 2006-2009 Seung-Jin Sul
//      Department of Computer Science
//      Texas A&M University
//      Contact: sulsj@cs.tamu.edu
//
//      CLASS IMPLEMENTATION
//      CHashFunc: Class for Universal hash functions
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details (www.gnu.org).
//
//*****************************************************************/

#include <iostream>
#include <cassert>

#include "hashfunc.hh"
#include "RandomLib/Random.hpp"


//// constructor
CHashFunc::CHashFunc(
    size_t t,
    size_t n,
    size_t c)
    : _m1(0), _m2(0), _t(t), _n(n), _a1(NULL), _a2(NULL), _c(c)
{
    UHashfunc_init(t, n, c);
}

//// destructor
CHashFunc::~CHashFunc()
{
    delete[] _a1;
    delete[] _a2;
}

void
CHashFunc::UHashfunc_init(
    size_t t,
    size_t n,
    size_t c)
{
    //// Init member variables
    _t = t;
    _n = n;
    _c = c;
    //// Get the prime number which is larger than t*n
    uint64_t top = _t * _n;
    uint64_t p = 0;
    size_t mul = 1;
    do {
        size_t from = 100 * mul;
        p = GetPrime(top, from);
        ++mul;
    }
    while (p == 0);
    _m1 = p; /// t*n ~~ m1
    uint64_t top2 = _c * _t * _n;
    uint64_t p2 = 0;
    mul = 1;
    do {
        unsigned from = 100 * mul;
        p2 = GetPrime(top2, from);
        ++mul;
    }
    while (p2 == 0);
    _m2 = p2; /// m2 > c*t*n --> to avoid double collision ==> I just use _c*p for _m2
    //// universal hash funciton = a * b mod m
    //// where B=(b1, b2,...,bn) is bit-string
    _a1 = new uint64_t [_n];
    _a2 = new uint64_t [_n];
    //// generate n random numbers between 0 and m1-1
    //// for hash function1 and hash function2
    //// rand() % 48
    //// random number between 0~47
    RandomLib::Random rnd; // r created with random seed
    for (size_t i = 0; i < _n; ++i) {
        _a1[i] = rnd.Integer<uint64_t>(_m1 - 1);
        _a2[i] = rnd.Integer<uint64_t>(_m2 - 1);
    }
}


void
CHashFunc::UHashfunc_init(
    size_t t,
    size_t n,
    size_t c,
    size_t newseed)
{
    //// Init member variables
    _t = t;
    _n = n;
    _c = c;
    //// Get the prime number which is larger than t*n
    uint64_t top = _t * _n;
    uint64_t p = 0;
    size_t mul = 1;
    do {
        size_t from = 100 * mul;
        p = GetPrime(top, from);
        ++mul;
    }
    while (p == 0);
    _m1 = p; //// t*n ~~ m1
    uint64_t top2 = _c * _t * _n;
    uint64_t p2 = 0;
    mul = 1;
    do {
        unsigned from = 100 * mul;
        p2 = GetPrime(top2, from);
        ++mul;
    }
    while (p2 == 0);
    _m2 = p2; //// m2 > c*t*n --> to avoid double collision ==> I just use _c*p for _m2
    //// universal hash funciton = a * b mod m
    //// where B=(b1, b2,...,bn) is bit-string
    _a1 = new uint64_t [_n];
    _a2 = new uint64_t [_n];
    //// generate n random numbers between 0 and m1-1
    //// for hash function1 and hash function2
    //// rand() % 48
    //// random number between 0~47
    //RandomLib::Random rnd; // r created with random seed
    RandomLib::Random rnd(newseed);     // r created with random seed
    std::cout << "    Random seed set to " << rnd.SeedString() << std::endl;
    for (size_t i = 0; i < _n; ++i) {
        _a1[i] = rnd.Integer<uint64_t>(_m1 - 1);
        _a2[i] = rnd.Integer<uint64_t>(_m2 - 1);
        //std::cout << "_a1[i] & _a2[i] = " << _a1[i] << " " << _a2[i] << std::endl;
    }
}


void
CHashFunc::UHashFunc(
    HV_STRUCT_T &hv,
    uint64_t bs,
    size_t numBits)
{
    hv.hv1 = 0;
    hv.hv2 = 0;
    for (size_t i = 0; i < numBits; i++) {
        if (bs & (1 << i)) {
            hv.hv1 += _a1[i];
            hv.hv2 += _a2[i];
        }
    }
    hv.hv1 %= _m1;
    hv.hv2 %= _m2;
}

uint64_t
CHashFunc::GetPrime(uint64_t topNum, size_t from)
{
    uint64_t primeNum = 0;
    uint64_t candidate = 0;
    if (topNum <= 100)
        candidate = 2;
    else
        candidate = topNum;
    while (candidate <= topNum + from) {
        uint64_t trialDivisor = 2;
        int prime = 1;
        while (trialDivisor * trialDivisor <= candidate) {
            if (candidate % trialDivisor == 0) {
                prime = 0;
                break;
            }
            trialDivisor++;
        }
        if (prime) primeNum = candidate;
        candidate++;
    }
    return primeNum;
}
//// eof
