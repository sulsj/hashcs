//*****************************************************************/
//
// Copyright (C) 2006-2009 Seung-Jin Sul
//      Department of Computer Science
//      Texas A&M University
//      Contact: sulsj@cs.tamu.edu
//
//      CLASS DEFINITION
//      CHashFunc: Universal hash functions
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details (www.gnu.org).
//
//*****************************************************************/


#ifndef HASHFUNC_HH
#define HASHFUNC_HH

#include <iostream>

typedef struct {
    uint64_t hv1;
    uint64_t hv2;
} HV_STRUCT_T;

class CHashFunc {
    uint64_t    _m1; // prime number1 for hash function1
    uint64_t    _m2; // prime number1 for hash function2
    size_t      _t; // number of trees
    size_t      _n; // number of taxa
    uint64_t    *_a1; // random numbers for hash function1
    uint64_t    *_a2; // random numbers for hash function2
    size_t      _c; // double collision factor: constant for c*t*n of hash function2;

public:

    CHashFunc() : _m1(0), _m2(0), _t(0), _n(0), _a1(NULL), _a2(NULL), _c(0) {
    }
    CHashFunc(size_t t, size_t n, size_t c);
    ~CHashFunc();

    void UHashfunc_init(size_t t, size_t n, size_t c);
    void UHashfunc_init(size_t t, size_t n, size_t c, size_t newSeed);
    void UHashFunc(HV_STRUCT_T &hv, uint64_t bs, size_t num_taxa); // with type3

    // Implicit bp

    uint64_t getA1(size_t idx) {
        return (_a1[idx]);
    }

    uint64_t getA2(size_t idx) {
        return (_a2[idx]);
    }

    uint64_t getM1() {
        return _m1;
    }

    uint64_t getM2() {
        return _m2;
    }

    uint64_t GetPrime(uint64_t top_num, size_t from);
};

#endif
