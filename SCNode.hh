#ifndef __SCNode_hh__
#define __SCNode_hh__

#include <vector>
#include <map>
#include <fstream>

using namespace std;

class SCNode {
public:

    SCNode();
    ~SCNode();

    string name;
    SCNode *parent;
    vector<SCNode*> children;

    // obsolete
    size_t support;
    double bl;

    bool IsLeaf();
    bool IsRoot();
    void SetDistance(double distance);
    double GetDistance() const;
    void ClearChildren();

    size_t NumChildren();
    void DrawOnTerminal(int dp, bool distances = false);

};


#endif


